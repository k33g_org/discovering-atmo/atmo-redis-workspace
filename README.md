# Atmo Workspace with Redis

## What is Atmo?

**[Atmo](https://github.com/suborbital/atmo)** is a project from [Suborbital](https://suborbital.dev/). 

To say it short, Atmo is a toolchain allowing writing and deploying wasm microservices smoothly.
With Atmo, you can write microservices in Rust, Swift, and AssemblyScript without worrying about complicated things, and you only have to care about your source code:

- No fight with wasm and string data type
- No complex toolchain to install
- Easy deployment with Docker (and then it becomes trivial to deploy wasm microservices on Kubernetes)
- ...

## About this project

This project is a **running demo** of 2 **Atmo** microservices using a **Redis** database:

- `./services/create` allowing to create a `{key:value}` record in the database
- `./services/read` allowing to read the `value` of record by providing its `key`


 This project provides all that you need to build and run this Atmo/Redis demo right now without installing anything. So, **to get the benefit of that, you have to open this project with [Gitpod](https://www.gitpod.io/)**.


### 🤚 To begin, click on the **Gitpod** button: [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/k33g_org/discovering-atmo/atmo-redis-workspace)


## 1️⃣ [Step 01] The "parts of interest" of the project (good to know)

### 🐳 Docker Compose

To run this project, we use **Docker Compose**. This is the `docker-compose.yml` file:

```yaml
services:
  redis-server:
    image: redis:latest
    container_name: redis-server
    ports:
      - 6379:6379

  atmo-server:
    image: suborbital/atmo:latest
    container_name: atmo-server
    entrypoint: atmo
    environment:
      - ATMO_HTTP_PORT=8080
    ports:
      - 8080:8080
    volumes:
      - /workspace/atmo-redis-workspace/services:/home/atmo
```

🖐️ the value of `container_name` (in the `redis-server` section) is `redis-server`, so, in the `./services/Directive.yaml` file, so, the **Redis** connection is defined like below:

```yaml
connections:
  redis:
    serverAddress: redis-server:6379
```

### 🌍 The `create` microservice/runnable

> extract of `services/create/src/lib.rs`
```rust
use suborbital::runnable::*;
use suborbital::cache;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct RequestMessage {
    key: String,
    value: String
}

struct Create{}

impl Runnable for Create {
    fn run(&self, input: Vec<u8>) -> Result<Vec<u8>, RunErr> {
        let in_string = String::from_utf8(input).unwrap();
    
        // read the JSON message
        let request_message: RequestMessage = serde_json::from_str(&in_string).unwrap();

        // create the key/value record
        cache::set(&request_message.key, request_message.value.as_bytes().to_vec(), 0);
    
        Ok(String::from(format!("Record key:{} value:{}\n", request_message.key, request_message.value)).as_bytes().to_vec())
    }
}
```

**Remarks**:
- `suborbital::cache` is used to connect to the Redis database
- `serde` is used to serialize and deserialize JSON (objects and strings)
- `cache::set` allows to create a record in the Redis database
- 🖐️ there is an additional dependency (**serde** to read the JSON values) in `Cargo.toml`:
   ```toml
   [dependencies]
   suborbital = '0.12.0'
   serde = { version = "1.0", features = ["derive"] }
   serde_json = "1.0"
   ```
- the associated handler in `Directive.yaml` is:
  ```
   handlers:
   - type: request
      resource: /create
      method: POST
      steps:
         - fn: create
  ```

### 🌍 The `read` microservice/runnable

> extract of `services/read/src/lib.rs`
```rust
use suborbital::runnable::*;
use suborbital::req;
use suborbital::cache;

struct Read{}

impl Runnable for Read {
    fn run(&self, _: Vec<u8>) -> Result<Vec<u8>, RunErr> {

        let key = req::url_param("key");

        let value = cache::get(key.as_str()).unwrap_or_default();
        let string_value = String::from_utf8(value).unwrap();
        
        Ok(String::from(format!("hello {}", string_value)).as_bytes().to_vec())
    }
}
```
**Remarks**:
- `cache::get` allows to fetch the value associated to a key
- the associated handler in `Directive.yaml` is:
  ```
   handlers:
   - type: request
      resource: /read/:key
      method: GET
      steps:
         - fn: read
  ```


## 2️⃣ [Step 02] Build the Bundle

To build the Bundle, type the following commands and wait for some seconds:

```bash
cd services
subo build .
```

The **subo CLI** builds 2 **wasm** files `create.wasm` and `read.wasm`, then, generates a new file: `runnables.wasm.zip` at the root of the project containing `create.wasm`, `read.wasm` and `Directive.yaml`:

```bash
.
└── services-demo
   ├── Directive.yaml
   ├── Dockerfile
   ├── create
   │  ├── Cargo.lock
   │  ├── Cargo.toml
   │  ├── create.wasm ⬅️👋
   │  └── src
   │     └── lib.rs
   ├── read
   │  ├── Cargo.lock
   │  ├── Cargo.toml
   │  ├── read.wasm ⬅️👋
   │  └── src
   │     └── lib.rs
   └── runnables.wasm.zip ⬅️👋
```

## 3️⃣ [Step 03] Serve the Bundle

To serve the Bundle, type the command below (at the root of the project):

```bash
docker-compose up
```

This command starts the **Redis** server and the **Atmo** server

## 4️⃣ [Step 04] Test the services (Runnables)

### Create a record

Open a new terminal and run the command below:

```bash
data='{"key":"jane","value":"Jane Doe"}'
curl -d "${data}" \
   -H "Content-Type: application/json" \
   -X POST "http://localhost:8080/create"
```

🖐️ **Httpie** is installed in this workspace, so you can use this command:
```bash
http POST "http://localhost:8080/create" key="jane" value="Jane Doe"
```

The expected result is `Record key:jane value:Jane Doe`

> You can check the data directly to the Redis Server:
> ```bash
> docker exec -it redis-server /bin/sh
> redis-cli
> keys *
> ```
> *Type `ctrl+c` to exit the redis prompt, then `exit` to exit the redis container.*

### Read a record

To get the value related to a redis key, type the command below:

```bash
http GET "http://localhost:8080/read/jane"
```
The expected result is `hello Jane Doe`

That's all for today 🎉
